import json
import datetime

from flask import render_template, request, jsonify, abort, g, url_for
from flask_httpauth import HTTPBasicAuth
auth = HTTPBasicAuth()

from app import app, db
from app.models import Product, Cartorder, Cart, Users, Favorite


@app.route('/')
@app.route('/index')
def index():
    user = {'username': 'Vitectron'}
    return render_template('index.html', title='Home', user=user)


@app.route('/api/users', methods = ['POST'])
@auth.login_required
def new_user():
    app.logger.info('data: %r', request.data)
    app.logger.info('values: %r', request.values)
    app.logger.info('json: %r', request.json)
    app.logger.info('form: %r', request.form)

    form = request.data
    form = json.loads(form)

    username = form.get('username')
    password = form.get('password')
    if username is None or password is None:
        abort(400) # missing arguments

    if Users.query.filter_by(username = username).first() is not None:
        abort(400) # existing user

    user = Users(username = username)
    user.hash_password(password)
    db.session.add(user)
    db.session.commit()

    data = jsonify({
        'userId': user.id,
        'expiration': user.get_user_expiration(),
        'token': user.generate_auth_token(),
    })
    # location = {'Location': url_for('get_user', id = user.id, _external = True)}
    return data # , 201, location


@auth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    user = Users.verify_auth_token(username_or_token)
    if not user:
        # try to authenticate with username/password
        user = Users.query.filter_by(username = username_or_token).first()
        if not user or not user.verify_password(password):
            return False

    g.user = user
    return True


@app.route('/api/token', methods = ['GET', 'POST'])
@auth.login_required
def get_auth_token():
    app.logger.info('data: %r', request.data)
    app.logger.info('values: %r', request.values)
    app.logger.info('form: %r', request.form)
    app.logger.info('json: %r', request.json)


    token = g.user.generate_auth_token()
    data = {
        'token': token,
        'userId': g.user.get_user_id(),
        'expiration': g.user.get_user_expiration(),
    }
    return jsonify(data)


@app.route('/add_product', methods=['GET', 'POST'])
@auth.login_required
def add_product():
    app.logger.info('data: %r', request.data)
    app.logger.info('values: %r', request.values)
    app.logger.info('form: %r', request.form)
    app.logger.info('json: %r', request.json)

    form = json.loads(request.data)

    userId = form.get('userId')
    title = form.get('title')
    description = form.get('description')
    imageUrl = form.get('imageUrl')

    price = form.get('price')
    if price:
        price = round(float(price), 2)

    isFavorite = form.get('isFavorite')
    if isFavorite in ['true', '1', 't']:
        isFavorite = True
    else:
        isFavorite = False

    app.logger.info('userId: %r', userId)
    app.logger.info('title: %r', title)
    app.logger.info('price: %r', price)
    app.logger.info('description: %r', description)
    app.logger.info('image_url: %r', imageUrl)
    app.logger.info('isFavorite: %r', isFavorite)

    product = Product(
        user_id=userId, title=title, price=price, description=description,
        image_url=imageUrl, is_favorite=isFavorite)
    db.session.add(product)
    db.session.commit()

    data = {
        'id': product.id,
        'userId': userId,
        'title': title,
        'price': price,
        'description': description,
        'imageUrl': imageUrl,
        'isFavorite': isFavorite,
    }

    app.logger.info('data: %r', data)
    return jsonify(data)


@app.route('/get_products', methods=['GET', 'POST'])
@auth.login_required
def get_products():
    app.logger.info('data: %r', request.data)
    app.logger.info('values: %r', request.values)
    app.logger.info('form: %r', request.form)
    app.logger.info('json: %r', request.json)

    form = json.loads(request.data)

    userId = form.get('userId')
    filteryUser = form.get('filterByUser')
    if filteryUser in ['true', '1', 't', 1]:
        filteryUser = True
    else:
        filteryUser = None

    favorites = Favorite.query.all()

    data = Product.query.all()

    products = {}
    if filteryUser:
        for item in data:
            if item.user_id == int(userId):
                product = {
                    'title': item.title,
                    'price': item.price,
                    'description': item.description,
                    'imageUrl': item.image_url,
                    'isFavorite': False,
                }

                if favorites:
                    for favorite in favorites:
                        if favorite.user_id == int(userId) and \
                            favorite.product_id == item.id:
                            product['isFavorite'] = favorite.is_favorite

                products[str(item.id)] = product
    else:
        for item in data:
            product = {
                'title': item.title,
                'price': item.price,
                'description': item.description,
                'imageUrl': item.image_url,
                'isFavorite': False,
            }

            if favorites:
                for favorite in favorites:
                    if favorite.user_id == int(userId) and \
                        favorite.product_id == item.id:
                        product['isFavorite'] = favorite.is_favorite

            products[str(item.id)] = product

    app.logger.info('products %r', products)
    return jsonify(products)


@app.route('/update_product', methods=['GET', 'POST'])
@auth.login_required
def update_product():
    app.logger.info('data: %r', request.data)
    app.logger.info('values: %r', request.values)
    app.logger.info('form: %r', request.form)
    app.logger.info('json: %r', request.json)

    # form = request.form
    form = json.loads(request.data)

    productId = form.get('id')
    if productId:
       productId = int(productId)

    title = form.get('title')
    description = form.get('description')
    imageUrl = form.get('imageUrl')

    price = form.get('price')
    if price:
        price = round(float(price), 2)

    isFavorite = form.get('isFavorite')
    if isFavorite in ['true', '1', 't', 1]:
        isFavorite = True
    elif isFavorite in ['false', '0', 'f', 0]:
        isFavorite = False
    else:
        isFavorite = None

    app.logger.info('id: %r', productId)
    app.logger.info('title: %r', title)
    app.logger.info('price: %r', price)
    app.logger.info('description: %r', description)
    app.logger.info('image_url: %r', imageUrl)
    app.logger.info('isFavorite: %r', isFavorite)

    product = Product.query.filter_by(id=productId).first()
    app.logger.info('product: %r', product)

    somethingToUpdate = False
    if title:
        product.title = title
        somethingToUpdate = True

    if price:
        product.price = price
        somethingToUpdate = True

    if description:
        product.description = description
        somethingToUpdate = True

    if imageUrl:
        product.image_url = imageUrl
        somethingToUpdate = True

    if isFavorite != None:
        product.is_favorite = isFavorite
        somethingToUpdate = True

    if somethingToUpdate:
        app.logger.info('updating product')
        db.session.commit()

    data = {
        'id': productId,
        'title': title,
        'price': price,
        'description': description,
        'imageUrl': imageUrl,
        'isFavorite': isFavorite,
    }

    app.logger.info('data: %r', data)
    return jsonify(data)


@app.route('/delete_product', methods=['GET', 'POST'])
@auth.login_required
def delete_product():
    app.logger.info('data: %r', request.data)
    app.logger.info('values: %r', request.values)
    app.logger.info('form: %r', request.form)
    app.logger.info('json: %r', request.json)

    # form = request.form
    form = json.loads(request.data)

    productId = form.get('productId')
    if productId:
       productId = int(productId)

    userId = form.get('userId')
    if userId:
       userId = int(userId)

    app.logger.info('id: %r', productId)
    favorite = Favorite.query.filter_by(user_id=userId, product_id=productId).first()
    if favorite:
        db.session.delete(favorite)
        db.session.commit()


    product = Product.query.filter_by(id=productId).first()
    if product:
        db.session.delete(product)
        db.session.commit()

    return jsonify({'yes': 'deleted'})


@app.route('/add_order', methods=['GET', 'POST'])
@auth.login_required
def add_order():
    app.logger.info('data: %r', request.data)
    app.logger.info('values: %r', request.values)
    app.logger.info('form: %r', request.form)
    app.logger.info('json: %r', request.json)

    form = request.data
    app.logger.info('type: %r', type(form))
    form = json.loads(form)
    app.logger.info('type: %r', type(form))

    userId = form.get('userId')
    amount = form.get('amount')
    dateTime = form.get('dateTime')
    app.logger.info('amount: %r', amount)
    app.logger.info('dateTime: %r', dateTime)


    cartOrder = Cartorder(user_id=userId, amount=amount, datetime=dateTime)
    db.session.add(cartOrder)
    db.session.flush()
    db.session.refresh(cartOrder)
    cartOrderId = cartOrder.id

    cartProducts = form.get('products')
    app.logger.info('cartProducts: %r', cartProducts)
    if cartProducts:
        for cartProduct in cartProducts:
            cartOrderId = cartOrderId
            price = cartProduct.get('price')
            title = cartProduct.get('title')
            quantity = cartProduct.get('quantity')

            app.logger.info('title: %r', title)
            app.logger.info('price: %r', price)
            app.logger.info('quantity: %r', quantity)

            cart = Cart(
                cart_order_id = cartOrderId, price=price,
                title=title, quantity=quantity
            )
            db.session.add(cart)
            db.session.commit()

    data = {"id": cartOrderId}
    app.logger.info('data: %r', data)
    return jsonify(data)


@app.route('/get_orders', methods=['GET', 'POST'])
@auth.login_required
def get_orders():
    app.logger.info('data: %r', request.data)
    app.logger.info('values: %r', request.values)
    app.logger.info('form: %r', request.form)
    app.logger.info('json: %r', request.json)

    form = json.loads(request.data)
    userId = form.get('userId')

    cartData = Cart.query.all()
    products = []
    for item in cartData:
        app.logger.info('proooooooo: %r', item.cart_order.__dict__)
        cartProduct = {
            "id": item.id,
            "title": item.title,
            "price": item.price,
            "quantity": item.quantity,
            "cartOrderId": item.cart_order_id
        }
        products.append(cartProduct)

    app.logger.info('products: %r', products)

    cartOrderData = Cartorder.query.all()
    orders = {}
    for item in cartOrderData:
        if item.user_id == int(userId):
            app.logger.info('item: %r', item.__dict__)
            datee = item.datetime
            app.logger.info('date: %r', datee)
            app.logger.info('type: %r', datee.strftime("%m/%d/%Y, %H:%M:%S"))
            cart = item.cart
            app.logger.info('caaaaaaaaaaart: %r', cart.__dict__)
            order = {
                "id": item.id,
                "amount": item.amount,
                "dateTime": datee.isoformat(),
                "products": [],
            }

            for product in products:
                app.logger.info(product)
                if product['cartOrderId'] == item.id:
                   order['products'].append(product)

            orders[str(item.id)] = order

    # cartData = Cart.query.all()
    # orders = {}
    # products = []
    # for item in cartData:
    #     app.logger.info('proooooooo: %r', item.cart_order.__dict__)
    #     cartOrder = item.cart_order
    #     order = {
    #         "id": cartOrder.id,
    #         "amount": cartOrder.amount,
    #         "dateTime": cartOrder.datetime.isoformat(),
    #         "products": []
    #     }
    #     product = {
    #         "id": item.id,
    #         "title": item.title,
    #         "price": item.price,
    #         "quantity": item.quantity,
    #     }
    #     products.append(product)
    #     order['products'] = products
    #
    #     orders[str(cartOrder.id)] = order

    app.logger.info('orders %r', orders)
    return jsonify(orders)


@app.route('/update_favorite', methods=['GET', 'POST'])
@auth.login_required
def update_favorite():
    app.logger.info('data: %r', request.data)
    app.logger.info('values: %r', request.values)
    app.logger.info('form: %r', request.form)
    app.logger.info('json: %r', request.json)

    form = json.loads(request.data)
    userId = form.get('userId')
    productId = form.get('productId')

    isFavorite = form.get('isFavorite')
    if isFavorite in ['true', '1', 't', 1]:
        isFavorite = True
    elif isFavorite in ['false', '0', 'f', 0]:
        isFavorite = False
    else:
        isFavorite = None


    favorite = Favorite.query.filter_by(
        user_id=userId, product_id= productId).first()

    if favorite:
        favorite.is_favorite = isFavorite
        db.session.merge(favorite)
        db.session.commit()
    else:
        favorite = Favorite(
            user_id=userId, product_id= productId, is_favorite=isFavorite)
        db.session.add(favorite)
        db.session.commit()

    data = {
        "userId": userId,
        "productId": productId,
        "isFavorite": isFavorite,
    }

    app.logger.info('data %r', data)
    return jsonify(data)



