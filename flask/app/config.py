import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'

    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:@flask_db:5432/flask'
    SQLALCHEMY_TRACK_MODIFICATIONS = False # silence the deprecation warning
    SQLALCHEMY_ECHO=True
